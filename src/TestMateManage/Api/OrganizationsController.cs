﻿using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using TestMateManage.Helpers;
using TestMateManage.Models;
using TestMateManage.ViewModels;

namespace TestMateManage.Api
{
    public class OrganizationsController : ApiBaseController
    {
        private readonly TestMateDbContext _dbContext;

        public OrganizationsController(TestMateDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        public IActionResult All([FromHeader]string xauth)
        {
            var userId = Security.FromBase64(xauth);

            var entities = _dbContext.OrganizationRoles
                .Include(x => x.Organization)
                .Include(x => x.User)
                .Where(role => role.UserId == userId && role.Type == RoleType.Teacher)
                .ToList();

            var result = entities.Select(x => new OrganizationRoleViewModel(x));

            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return HttpNotFound();
            }
        }
    }
}
