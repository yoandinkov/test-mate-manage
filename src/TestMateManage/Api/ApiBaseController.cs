﻿using System.Web;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Filters;

namespace TestMateManage.Api
{
    [Route("/api/[controller]/[action]")]
    public class ApiBaseController : Controller
    {
        public override void OnActionExecuted(ActionExecutedContext context)
        {
        }
    }
}
