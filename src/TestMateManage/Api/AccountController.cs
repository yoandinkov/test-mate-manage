﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Mvc;
using TestMateManage.Helpers;
using TestMateManage.Models;
using Microsoft.Data.Entity;

namespace TestMateManage.Api
{
    public class AccountController : ApiBaseController
    {
        private readonly ITestMateDbContext _dbContext;
        private readonly UserManager<User> _userManager;

        public AccountController(ITestMateDbContext dbContext, UserManager<User> userManager)
        {
            _dbContext = dbContext;
            _userManager = userManager;
        }

        [HttpGet]
        public async Task<IActionResult> UserIdLocal([FromHeader]string xauth)
        {
            if (xauth == null)
            {
                return HttpBadRequest();
            }

            var decoded = Security.FromBase64(xauth).Split(':');

            if (decoded.Length != 2)
            {
                return HttpBadRequest();
            }

            var email = decoded[0];

            var password = decoded[1];

            var user = await _userManager.FindByEmailAsync(email);

            var passwordCorrect = await _userManager.CheckPasswordAsync(user, password);

            if (passwordCorrect)    
            {
                return Ok(user.Id);
            }

            Response.StatusCode = (int)HttpStatusCode.Unauthorized;

            return HttpBadRequest();
        }

        [HttpGet]
        public async Task<IActionResult> UserIdExternal([FromHeader]string xauth)
        {
            if (xauth == null)
            {
                return HttpBadRequest();
            }

            var decoded = Security.FromBase64(xauth).Split(':');

            if (decoded.Length != 2)
            {
                return HttpBadRequest();
            }

            var provider = decoded[0];

            var providerKey = decoded[1];

            var user = await _userManager.FindByLoginAsync(provider, providerKey);

            if (user != null)
            {
                return Ok(user.Id);
            }

            return HttpUnauthorized();
        }

        [HttpGet]
        public async Task<IActionResult> AuthorizeUser([FromHeader]string xauth, [FromHeader]string xtype, [FromHeader]string xorgid)
        {
            if (string.IsNullOrEmpty(xauth) || string.IsNullOrEmpty(xtype) || string.IsNullOrEmpty(xorgid))
            {
                return HttpBadRequest();
            }

            var userId = Security.FromBase64(xauth);

            int orgId;
            int roleTypeId;

            var parsedOrg = int.TryParse(Security.FromBase64(xorgid), out orgId);
            var parsedRole = int.TryParse(Security.FromBase64(xtype), out roleTypeId);

            if (parsedOrg == false || parsedRole == false)
            {
                return HttpBadRequest();
            }


            var roleType = (RoleType)roleTypeId;

            OrganizationRole userRole;

            if (roleType == RoleType.Both)
            {
                userRole =
                    await _dbContext.OrganizationRoles
                        .Include(x => x.User)
                        .Include(x => x.Organization)
                        .FirstOrDefaultAsync(x => x.OrganizationId == orgId && x.UserId == userId);
            }
            else
            {
                userRole =
                    await _dbContext.OrganizationRoles
                        .Include(x => x.User)
                        .Include(x => x.Organization)
                        .FirstOrDefaultAsync(x => x.OrganizationId == orgId && x.UserId == userId && x.Type == roleType);
            }   

            if (userRole == null)
            {
                return HttpBadRequest();
            }

            return Ok();
        }           
    }
}
