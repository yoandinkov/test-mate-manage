﻿namespace TestMateManage.Models
{
    public enum RoleType
    {
        Both = 0,

        Teacher = 1,

        Student = 2
    }
}
