﻿namespace TestMateManage.Models
{
    public class OrganizationRole
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public int OrganizationId { get; set; }
        
        public bool Approved { get; set; }

        public RoleType Type { get; set; }

        public virtual User User { get; set; }

        public virtual Organization Organization { get; set; }
    }
}
