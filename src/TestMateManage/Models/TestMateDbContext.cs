using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;

namespace TestMateManage.Models
{
    public interface ITestMateDbContext
    {
        DbSet<Organization> Organizations { get; set; }

        DbSet<User> Users { get; set; }

        DbSet<OrganizationRole> OrganizationRoles { get; set; }

        int SaveChanges();

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }

    public class TestMateDbContext : IdentityDbContext<User>, ITestMateDbContext
    {
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        public DbSet<Organization> Organizations { get; set; }

        public DbSet<OrganizationRole> OrganizationRoles { get; set; }
    }
}
