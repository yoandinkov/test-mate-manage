﻿using System;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Newtonsoft.Json;
using TestMateManage.Helpers;
using TestMateManage.Models;
using TestMateManage.ViewModels;

namespace TestMateManage.Controllers
{
    public class OrganizationController : BaseController
    {
        private readonly ITestMateDbContext _dbContext;

        public OrganizationController(ITestMateDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public ActionResult Index()
        {
            var userId = User.GetUserId();

            var dbData = _dbContext
                .OrganizationRoles
                .Include(x => x.Organization)
                .Include(x => x.User)
                .Where(x => x.UserId == userId)
                .ToList();

            var model = dbData.Select(x => new OrganizationRoleViewModel(x)).ToList();

            return View(model);
        }

        public ActionResult Create(OrganizationCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                var organization = new Organization()
                {
                    Description = model.Description,
                    Name = model.Name
                };

                _dbContext.Organizations.Add(organization);

                _dbContext.OrganizationRoles.Add(new OrganizationRole()
                {
                    Approved = true,
                    OrganizationId = organization.Id,
                    Type = RoleType.Teacher,
                    UserId = User.GetUserId()
                });

                _dbContext.SaveChanges();

                return RedirectToAction(nameof(Index));
            }

            return View();
        }

        [HttpGet]
        public IActionResult Test()
        {
            return Ok("secret");
        }

        public ActionResult List()
        {
            var userId = User.GetUserId();

            var userRoles = _dbContext
                .OrganizationRoles
                .Where(x => x.UserId == userId)
                .Select(y => y.OrganizationId).ToList();

            var organizations = _dbContext
                .Organizations
                .Where(org => userRoles.Contains(org.Id) == false)
                .ToList();

            var model = organizations.Select(org => new OrganizationViewModel(org)).ToList();

            return View(model);
        }

        public ActionResult Join(string data)
        {
            var inputRole = JsonConvert.DeserializeObject<OrganizationJoinViewModel>(data);

            var userId = User.GetUserId();

            var existingRole = _dbContext
                .OrganizationRoles
                .FirstOrDefault(x => x.OrganizationId == inputRole.OrganizationId && x.UserId == userId);

            if (existingRole != null)
            {
                throw new ArgumentException("Role already exists!");
            }

            var role = new OrganizationRole()
            {
                Approved = false,
                OrganizationId = inputRole.OrganizationId,
                UserId = userId,
                Type = inputRole.Type
            };


            _dbContext.OrganizationRoles.Add(role);

            _dbContext.SaveChanges();

            return RedirectToAction(nameof(Index));
        }

        public IActionResult Management(int id)
        {
            var userId = User.GetUserId();

            var role = _dbContext
                .OrganizationRoles
                .Include(x => x.Organization)
                .Include(x => x.User)
                .First(x => x.OrganizationId == id && x.UserId == userId);

            if (role == null || role.Approved == false)
            {
                return HttpBadRequest();
            }

            var roles = _dbContext
                .OrganizationRoles
                .Include(x => x.Organization)
                .Include(x => x.User)
                .OrderBy(x => x.Type)
                .ThenBy(x => x.User.UserName)
                .Where(x => x.OrganizationId == id && x.Id != role.Id && x.Approved == true).ToList();

            var model = new OrganizationManagementRoleViewModel
            {
                CurrentUser = new OrganizationRoleViewModel(role),
                Users = roles.Select(x => new OrganizationRoleViewModel(x)),
                Navigation = new OrganizationNavigationViewModel
                {
                    IsTeacher = role.Type == RoleType.Teacher,
                    OrgId = id 
                }
            };

            return View(model);
        }

        [HttpGet, MembersOnly(Type = RoleType.Teacher)]
        public IActionResult PendingMemberships(int id)
        {
            var orgId = id;

            var roles = _dbContext
                .OrganizationRoles
                .Include(x => x.Organization)
                .Include(x => x.User)
                .Where(x => x.OrganizationId == orgId && x.Approved == false).ToList();

            var memberships = roles.Select(x => new OrganizationRoleViewModel(x)).ToList();

            var userRole = _dbContext.OrganizationRoles.First(x => x.OrganizationId == orgId && x.UserId == User.GetUserId());

            var model = new OrganizationPendingMembershipsViewModel
            {
                Memberships = memberships,
                Navigation = new OrganizationNavigationViewModel
                {
                    IsTeacher = userRole.Type == RoleType.Teacher,
                    OrgId = orgId
                }
            };

            return View(model); 
        }

        [HttpPost, MembersOnly(Type = RoleType.Teacher)]
        public IActionResult ApproveMembership(int id, [FromBody]OrganizationRoleSimpleViewModel data)
        {
            var role = _dbContext.OrganizationRoles.FirstOrDefault(x => x.Id == data.RoleId);

            if (role == null)
            {
                return HttpNotFound();
            }

            if (role.OrganizationId != data.OrganizationId)
            {
                return HttpBadRequest();
            }

            role.Approved = true;

            _dbContext.SaveChanges();

            return Ok();
        }

        [HttpPost, MembersOnly(Type = RoleType.Teacher)]
        public IActionResult DenyMembership(int id, [FromBody]OrganizationRoleSimpleViewModel data)
        {
            var role = _dbContext.OrganizationRoles.FirstOrDefault(x => x.Id == data.RoleId);

            if (role == null)
            {
                return HttpNotFound();
            }

            if (role.OrganizationId != data.OrganizationId)
            {
                return HttpBadRequest();
            }

            _dbContext.OrganizationRoles.Remove(role);

            _dbContext.SaveChanges();

            return Ok();
        }
    }
}
