﻿using Microsoft.AspNet.Mvc;

namespace TestMateManage.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Organization");
            }

            return View();
        }
    }
}
