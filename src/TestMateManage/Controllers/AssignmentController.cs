﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNet.Mvc;
using TestMateManage.Helpers;
using TestMateManage.Models;
using TestMateManage.ViewModels;
using Microsoft.Data.Entity;
using System.Configuration;

namespace TestMateManage.Controllers
{
    public class AssignmentController : BaseController
    {
        private readonly ITestMateDbContext _dbContext;
        private readonly ApiClient _testMateDevelopClient;
        private readonly ApiClient _testMateEvaluateClient;
        public AssignmentController(ITestMateDbContext dbContext)
        {
            _dbContext = dbContext;
            _testMateDevelopClient = new ApiClient("http://test-mate-develop.herokuapp.com/api/");
            _testMateEvaluateClient = new ApiClient("http://test-mate-evaluate.herokuapp.com/api/");
        }

        [HttpGet]
        public async Task<IActionResult> Create(int id)
        {
            var orgId = id;

            var userId = User.GetUserId();

            var templates = await _testMateDevelopClient.Get<IEnumerable<TemplateApiViewModel>>("templates/", new Dictionary<string, string>() {
                { "xauth", Security.ToBase64(userId) },
                { "xorgid", Security.ToBase64(orgId) }
            });

            var roles = _dbContext
                .OrganizationRoles
                .Include(x => x.User)
                .Include(x => x.Organization)
                .Where(x => x.OrganizationId == orgId && x.UserId != userId).ToList();

            var users = roles.OrderBy(x => x.Type).Select(x => new OrganizationRoleViewModel(x));

            var userRole = _dbContext.OrganizationRoles.Single(x => x.OrganizationId == id && x.UserId == userId);

            var model = new AssignmentCreateViewModel
            {
                Templates = templates,
                Users = users,
                Navigation = new OrganizationNavigationViewModel
                {
                    IsTeacher = userRole.Type == RoleType.Teacher,
                    OrgId = orgId
                }
            };

            return View(model);
        }


        [HttpPost, MembersOnly(Type = RoleType.Teacher)]
        public async Task<IActionResult> SaveAssignment(int id, [FromBody]AssignmentSaveViewModel data)
        {
            var dbRoles = _dbContext
                .OrganizationRoles
                .Include(x => x.User)
                .Include(x => x.Organization)
                .Where(x => x.OrganizationId == id && data.UserIds.Contains(x.UserId))
                .ToList();

            var roles = dbRoles.Select(x => new OrganizationRoleViewModel(x));

            foreach (var role in roles)
            {
                foreach (var template in data.Templates)
                {
                    var response = await _testMateEvaluateClient.Post<ApiResponse>("/api/assignment", new Dictionary<string, string>
                    {
                        { "assignee_id", Security.ToBase64(role.UserId) },
                        { "template_id", template.Id.ToString() },
                        { "template_title", template.Title },
                        { "organization_id", role.Id.ToString() },
                        { "organization_name", role.Name },
                        { "assigner_id", Security.ToBase64(User.GetUserId()) }
                    });

                }
            }

            return Ok();
        }

        [HttpGet, MembersOnly(Type = RoleType.Teacher)]
        public async Task<IActionResult> DoneAssignments(int id)
        {
            var url = string.Format("/api/done-evaluations/{0}", id);

            var response = await _testMateEvaluateClient.Get<IEnumerable<EvaluationApiResponse>>(url, new Dictionary<string, string>
            {
                { "xauth", Security.ToBase64(User.GetUserId()) }
            });

            var users =
                _dbContext.OrganizationRoles.Include(x => x.User).Where(x => x.OrganizationId == id).ToList();

            var assignments = response.Select(x => new DoneAssignmentViewModel(x)).ToList();

            foreach (var evaluation in assignments)
            {
                evaluation.Username = users.First(x => x.UserId == evaluation.UserId).User.UserName;
            }

            var userRole = _dbContext.OrganizationRoles.Single(x => x.OrganizationId == id && x.UserId == User.GetUserId());

            var model = new DoneAssignmentsViewModel
            {
                Assignments = assignments,
                Navigation = new OrganizationNavigationViewModel
                {
                    IsTeacher = userRole.Type == RoleType.Teacher,
                    OrgId = id
                }
            };

            return View(model);
        }

        [HttpGet, MembersOnly(Type = RoleType.Teacher)]
        public async Task<IActionResult> Check(int id, int evaluationId, string templateId)
        {
            var evaluationResultUrl = $"/api/assignment/result/{evaluationId}";

            var evaluationResult = await _testMateEvaluateClient.Get<Dictionary<string, IEnumerable<string>>>(evaluationResultUrl, new Dictionary<string, string>
            {
                { "xauth", Security.ToBase64(User.GetUserId()) }
            });

            var template = await _testMateDevelopClient.Get<TemplateFullApiViewModel>($"/api/templates/assignment-teacher/{templateId}", new Dictionary<string, string>
            {
                { "xauth", Security.ToBase64(User.GetUserId()) },
                { "xorgid", Security.ToBase64(id) },
            });

            var evaluation = AI.CheckEvaluation(template, evaluationResult);

            var userRole = _dbContext.OrganizationRoles.First(x => x.OrganizationId == id && x.UserId == User.GetUserId());

            var model = new AssignmentCheckViewModel
            {
                Evaluation = evaluation,
                Navigation = new OrganizationNavigationViewModel
                {
                    OrgId = id,
                    IsTeacher = userRole.Type == RoleType.Teacher
                },
                EvaluationId = evaluationId,
                AssignerId = userRole.UserId
            };

            return View(model);
        }

        [HttpPost, MembersOnly(Type = RoleType.Teacher)]
        public async Task<IActionResult> Save(int id, [FromBody]CheckedEvaluationApiModel input)
        {
            if (input == null)
            {
                return HttpBadRequest();
            }

            var evaluationResultUrl = $"/api/assignment/save-result/{input.EvaluationId}";

            var response = await _testMateEvaluateClient.Post<ApiResponse>(evaluationResultUrl, new Dictionary<string, string>
            {
                { "score", input.Score.ToString() }
            },
            new Dictionary<string, string>
            {
                { "xauth", Security.ToBase64(User.GetUserId()) }
            });

            if (response.Status == System.Net.HttpStatusCode.OK)
            {
                return Ok();
            }
            else
            {
                return HttpBadRequest(response);
            }
        }

        [HttpGet, MembersOnly(Type = RoleType.Teacher)]
        public async Task<IActionResult> History(int id, [FromQuery]string userId)
        {
            var evaluationResultUrl = $"/api/history/{id}/{userId}";

            var assignments = await _testMateEvaluateClient.Get<IEnumerable<AssignmentHistoryFullViewModel>>(evaluationResultUrl, new Dictionary<string, string>
            {
                { "xauth", Security.ToBase64(User.GetUserId()) }
            });

            var userRole = _dbContext.OrganizationRoles.First(x => x.OrganizationId == id && x.UserId == User.GetUserId());

            var model = new AssignmentHistoryViewModel
            {
                Assignments = assignments,
                Navigation = new OrganizationNavigationViewModel
                {
                    OrgId = id,
                    IsTeacher = userRole.Type == RoleType.Teacher
                }
            };

            return View(model);
        }

        [HttpGet, MembersOnly(Type = RoleType.Both)]
        public async Task<IActionResult> PersonalHistory(int id)
        {
            var evaluationResultUrl = $"/api/my-history/{id}";

            var assignments = await _testMateEvaluateClient.Get<IEnumerable<AssignmentHistoryFullViewModel>>(evaluationResultUrl, new Dictionary<string, string>
            {
                { "xauth", Security.ToBase64(User.GetUserId()) }
            });

            var userRole = _dbContext.OrganizationRoles.First(x => x.OrganizationId == id && x.UserId == User.GetUserId());

            var model = new AssignmentHistoryViewModel
            {
                Assignments = assignments,
                Navigation = new OrganizationNavigationViewModel
                {
                    OrgId = id,
                    IsTeacher = userRole.Type == RoleType.Teacher
                }
            };

            return View(model);
        }

    }
}
