﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TestMateManage.Helpers
{
    public class ApiClient
    {
        public ApiClient(string url)
        {
            Url = url;
        }

        public string Url { get; set; }

        public async Task<T> Get<T>(string path, Dictionary<string, string> headers = null)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                if (headers != null)
                {
                    foreach (var pair in headers)
                    {
                        client.DefaultRequestHeaders.Add(pair.Key, pair.Value);
                    }
                }

                HttpResponseMessage response = await client.GetAsync(path);
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsStringAsync();

                    return JsonConvert.DeserializeObject<T>(result);
                }

                return default(T);
            }
        }

        public async Task<T> Post<T>(string path, Dictionary<string, string> content, Dictionary<string, string> headers = null)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                if (headers != null)
                {
                    foreach (var pair in headers)
                    {
                        client.DefaultRequestHeaders.Add(pair.Key, pair.Value);
                    }
                }
                var postDataPairs = new List<KeyValuePair<string, string>>();

                foreach (var pair in content)
                {
                    postDataPairs.Add(new KeyValuePair<string, string>(pair.Key, pair.Value));
                }

                var data = new FormUrlEncodedContent(postDataPairs);

                HttpResponseMessage response = await client.PostAsync(path, data);
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsStringAsync();

                    return JsonConvert.DeserializeObject<T>(result);
                }

                return default(T);
            }
        }
    }
}
