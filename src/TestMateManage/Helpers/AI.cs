﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestMateManage.ViewModels;

namespace TestMateManage.Helpers
{
    public static class AI
    {
        public static TemplateCheckedViewModel CheckEvaluation(TemplateFullApiViewModel template, Dictionary<string, IEnumerable<string>> result)
        {
            var checkedTemplate = new TemplateCheckedViewModel();

            checkedTemplate.Title = template.Title;
            checkedTemplate.Description = template.Description;

            foreach (var templateQuestion in template.Questions)
            {
                var checkedQuestion = new QuestionCheckedViewModel();

                checkedQuestion.Text = templateQuestion.Text;
                checkedQuestion.Id = templateQuestion.Id;
                checkedQuestion.Order = templateQuestion.Order;
                checkedQuestion.Weight = templateQuestion.Weight;
                checkedQuestion.Type = templateQuestion.Type;

                checkedQuestion.Score = GetScore(templateQuestion, result[templateQuestion.Id]);
                checkedQuestion.Answers = GetAnswers(templateQuestion, result[templateQuestion.Id]);

                if (templateQuestion.Type == QuestionType.FreeText)
                {
                    checkedQuestion.TextAnswer = result[templateQuestion.Id].Single();
                }

                checkedTemplate.Questions.Add(checkedQuestion);
            }

            return checkedTemplate;
        }

        private static IEnumerable<AnswerCheckedViewModel> GetAnswers(QuestionApiViewModel question, IEnumerable<string> selectedAnswerIds)
        {
            foreach (var answer in question.Answers)
            {
                yield return new AnswerCheckedViewModel
                {
                    Order = answer.Order,
                    Text = answer.Text,
                    Right = answer.Right,
                    Selected = selectedAnswerIds.Contains(answer.Id)
                };
            }
        }

        private static double GetScore(QuestionApiViewModel question, IEnumerable<string> selectedAnswerIds)
        {
            double value = 0;

            switch (question.Type)
            {
                case QuestionType.Single:

                    var answerId = selectedAnswerIds.Single();

                    var templateAnswer = question.Answers.Single(x => x.Id == answerId);

                    if (templateAnswer.Right)
                    {
                        value = question.Weight;
                    }

                    break;

                case QuestionType.Multiple:
                    var answerWeight = (double)question.Weight / question.Answers.Count(a => a.Right);

                    foreach (var selectedAnswer in selectedAnswerIds)
                    {
                        var answer = question.Answers.Single(x => x.Id == selectedAnswer);

                        if (answer.Right)
                        {
                            value += answerWeight;
                        }
                        else
                        {
                            value -= answerWeight;
                        }
                    }

                    if (value < 0)
                    {
                        value = 0;
                    }

                    break;
            }

            return value;
        }
    }
}
