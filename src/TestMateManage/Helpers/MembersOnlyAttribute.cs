﻿using System;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Filters;
using TestMateManage.Models;

namespace TestMateManage.Helpers
{
    public class MembersOnlyAttribute : ActionFilterAttribute
    {
        private ITestMateDbContext _dbContext;

        public RoleType Type
        {
            get; set;
        }

        public MembersOnlyAttribute()
        {
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var userId = filterContext.HttpContext.User.GetUserId();

            _dbContext = GetDbContext(filterContext.Controller);

            var actionArgument = Convert.ToString(filterContext.ActionArguments["id"]);

            var orgId = Convert.ToInt32(actionArgument);

            var role = _dbContext.OrganizationRoles.First(x => x.UserId == userId && x.OrganizationId == orgId);

            if (role == null || role.Approved == false)
            {
                filterContext.Result = new BadRequestObjectResult("Not allowed");
            }

            if (Type != RoleType.Both && role.Type != Type)
            {
                filterContext.Result = new BadRequestObjectResult("Role not allowed");
            }
        }

        private ITestMateDbContext GetDbContext(object controller)
        {
            var controllerType = controller.GetType();

            var dbContextField = controllerType.GetField("_dbContext", BindingFlags.NonPublic | BindingFlags.Instance);

            var dbContext = dbContextField.GetValue(controller) as ITestMateDbContext;

            return dbContext;
        }
    }
}
