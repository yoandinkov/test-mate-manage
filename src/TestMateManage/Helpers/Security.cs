﻿using System;
using System.Text;

namespace TestMateManage.Helpers
{
    public static class Security
    {
        public static string FromBase64(string encodedString)
        {
            byte[] data = Convert.FromBase64String(encodedString);

            string decodedString = Encoding.UTF8.GetString(data);

            return decodedString;
        }

        public static string ToBase64(object decodedString)
        {
            byte[] data = Encoding.UTF8.GetBytes(decodedString.ToString());

            return Convert.ToBase64String(data);
        }
    }
}
