﻿using Newtonsoft.Json;

namespace TestMateManage.ViewModels
{
    public class EvaluationApiResponse
    {
        [JsonProperty("assignee_id")]
        public string UserId { get; set; }

        [JsonProperty("template_id")]
        public string TemplateId { get; set; }

        [JsonProperty("template_title")]
        public string TemplateTitle { get; set; }

        [JsonProperty("evaluation_id")]
        public int EvaluationId { get; set; }
    }
}
