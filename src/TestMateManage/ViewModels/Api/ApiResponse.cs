﻿using System.Net;
using Newtonsoft.Json;

namespace TestMateManage.ViewModels
{
    public class ApiResponse
    {
        [JsonProperty("status")]
        public HttpStatusCode Status { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
