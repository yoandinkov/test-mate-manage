﻿using System.Collections.Generic;

namespace TestMateManage.ViewModels
{
    public class AssignmentCreateViewModel
    {
        public OrganizationNavigationViewModel Navigation { get; internal set; }
        public IEnumerable<TemplateApiViewModel> Templates { get; set; }

        public IEnumerable<OrganizationRoleViewModel> Users { get; set; }
    }
}
