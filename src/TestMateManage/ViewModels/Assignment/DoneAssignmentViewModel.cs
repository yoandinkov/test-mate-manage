﻿namespace TestMateManage.ViewModels
{
    public class DoneAssignmentViewModel
    {
        public DoneAssignmentViewModel()
        {

        }

        public DoneAssignmentViewModel(EvaluationApiResponse domain)
        {
            UserId = domain.UserId;

            TemplateId = domain.TemplateId;

            TemplateTitle = domain.TemplateTitle;

            EvaluationId = domain.EvaluationId;
        }

        public string UserId { get; set; }

        public string Username { get; set; }

        public string TemplateId { get; set; }

        public int EvaluationId { get; set; }

        public string TemplateTitle { get; set; }
    }
}
