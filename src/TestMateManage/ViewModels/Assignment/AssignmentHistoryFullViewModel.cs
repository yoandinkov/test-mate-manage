﻿using System;
using Newtonsoft.Json;

namespace TestMateManage.ViewModels
{
    public class AssignmentHistoryFullViewModel
    {
        [JsonProperty("template_title")]
        public string TemplateTitle { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("score")]
        public double? Score { get; set; }

        [JsonProperty("max_score")]
        public double? MaxScore { get; set; }

        [JsonProperty("date_assigned")]
        public DateTime? DateAssigned { get; set; }

        [JsonProperty("date_evaluation_start")]
        public DateTime? DateEvaluationStart { get; set; }

        [JsonProperty("date_evaluation_finish")]
        public DateTime? DateEvaluationFinish { get; set; }

        [JsonProperty("date_checked")]
        public DateTime? DateChecked { get; set; }
    }
}
