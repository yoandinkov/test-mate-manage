﻿using System.Collections.Generic;

namespace TestMateManage.ViewModels
{
    public class AssignmentHistoryViewModel
    {
        public OrganizationNavigationViewModel Navigation { get; set; }

        public IEnumerable<AssignmentHistoryFullViewModel> Assignments { get; set; }
    }
}
