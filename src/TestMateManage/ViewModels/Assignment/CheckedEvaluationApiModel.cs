﻿using Newtonsoft.Json;

namespace TestMateManage.ViewModels
{
    public class CheckedEvaluationApiModel
    {
        [JsonProperty("id")]
        public int EvaluationId { get; set; }

        [JsonProperty("score")]
        public double Score { get; set; }
    }
}
