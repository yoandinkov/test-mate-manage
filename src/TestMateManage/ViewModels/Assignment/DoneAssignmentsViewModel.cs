﻿using System.Collections.Generic;

namespace TestMateManage.ViewModels
{
    public class DoneAssignmentsViewModel
    {
        public IEnumerable<DoneAssignmentViewModel> Assignments { get; set; }

        public OrganizationNavigationViewModel Navigation { get; set; }
    }
}
