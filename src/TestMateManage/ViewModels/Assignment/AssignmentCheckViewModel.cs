﻿namespace TestMateManage.ViewModels
{
    public class AssignmentCheckViewModel
    {
        public string AssignerId { get; set; }

        public int EvaluationId { get; set; }

        public TemplateCheckedViewModel Evaluation  { get; set; }

        public OrganizationNavigationViewModel Navigation { get; set; }
    }
}
