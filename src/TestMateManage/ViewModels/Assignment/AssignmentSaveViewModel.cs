﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace TestMateManage.ViewModels
{
    public class AssignmentSaveViewModel
    {
        [JsonProperty("userIds")]
        public List<string> UserIds { get; set; }
        
        [JsonProperty("templateIds")]
        public List<TemplateApiViewModel> Templates { get; set; }
    }
}
