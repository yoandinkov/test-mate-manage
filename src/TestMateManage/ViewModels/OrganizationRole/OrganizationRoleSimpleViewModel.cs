﻿using Newtonsoft.Json;

namespace TestMateManage.ViewModels
{
    public class OrganizationRoleSimpleViewModel
    {
        [JsonProperty("orgId")]
        public int OrganizationId { get; set; }

        [JsonProperty("roleId")]
        public int RoleId { get; set; }
    }
}
