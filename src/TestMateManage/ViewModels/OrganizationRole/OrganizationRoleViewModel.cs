﻿using TestMateManage.Models;

namespace TestMateManage.ViewModels
{
    public class OrganizationRoleViewModel
    {
        public string Name { get; set; }

        public int Id { get; set; }

        public RoleType Type { get; set; }

        public bool Approved { get; set; }

        public string Username { get; set; }

        public string UserId { get; set; }

        public int RoleId { get; set; }

        public OrganizationRoleViewModel(OrganizationRole domain)
        {
            Id = domain.OrganizationId;

            Name = domain.Organization.Name;

            Type = domain.Type;

            Approved = domain.Approved;

            Username = domain.User.Email;

            UserId = domain.User.Id;

            RoleId = domain.Id;
        }
    }
}
