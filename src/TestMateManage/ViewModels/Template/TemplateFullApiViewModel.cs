﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace TestMateManage.ViewModels
{
    public class TemplateFullApiViewModel
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("questions")]
        public IEnumerable<QuestionApiViewModel> Questions { get; set; }
    }

    public class QuestionApiViewModel
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("type")]
        public QuestionType Type { get; set; }

        [JsonProperty("order")]
        public int Order { get; set; }

        [JsonProperty("weight")]
        public int Weight { get; set; }

        [JsonProperty("answers")]
        public IEnumerable<AnswerApiViewModel> Answers { get; set; }
    }

    public class AnswerApiViewModel
    {

        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("order")]
        public int Order { get; set; }

        [JsonProperty("right")]
        public bool Right { get; set; }
    }

    public enum QuestionType
    {
        Single  = 1,

        Multiple = 2,

        FreeText = 3
    }
}