﻿using System.Collections.Generic;

namespace TestMateManage.ViewModels
{
    public class TemplateCheckedViewModel
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public IList<QuestionCheckedViewModel> Questions { get; set; }

        public TemplateCheckedViewModel()
        {
            Questions = new List<QuestionCheckedViewModel>();
        }
    }

    public class QuestionCheckedViewModel
    {
        public string Id { get; set; }
        
        public string Text { get; set; }
        
        public QuestionType Type { get; set; }

        public int Order { get; set; }
        
        public int Weight { get; set; }

        public double Score { get; set; }

        public IEnumerable<AnswerCheckedViewModel> Answers { get; set; }

        public string TextAnswer { get; set; }
    }

    public class AnswerCheckedViewModel
    {
        public int Order { get; set; }

        public bool Right { get; set; }

        public bool Selected { get; set; }

        public string Text { get; set; }
    }
}
