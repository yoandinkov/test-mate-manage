﻿using Newtonsoft.Json;

namespace TestMateManage.ViewModels
{
    public class TemplateApiViewModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }
    }
}
