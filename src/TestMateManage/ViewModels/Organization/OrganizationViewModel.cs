﻿namespace TestMateManage.ViewModels
{
    public class OrganizationViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public OrganizationViewModel(Models.Organization domain)
        {
            Id = domain.Id;

            Name = domain.Name;
        }
    }
}
