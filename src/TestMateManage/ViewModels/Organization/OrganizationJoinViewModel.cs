﻿using System.ComponentModel.DataAnnotations;
using TestMateManage.Models;

namespace TestMateManage.ViewModels
{
    public class OrganizationJoinViewModel
    {
        public RoleType Type { get; set; }

        [Range(0, int.MaxValue)]
        public int OrganizationId { get; set; }
    }
}
