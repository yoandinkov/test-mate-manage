﻿namespace TestMateManage.ViewModels
{
    public class OrganizationNavigationViewModel
    {
        public int OrgId { get; set; }

        public bool IsTeacher { get; set; }
    }
}
