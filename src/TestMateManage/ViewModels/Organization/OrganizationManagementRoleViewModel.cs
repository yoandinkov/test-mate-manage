﻿using System.Collections.Generic;

namespace TestMateManage.ViewModels
{
    public class OrganizationManagementRoleViewModel
    {
        public OrganizationRoleViewModel CurrentUser { get; set; }

        public IEnumerable<OrganizationRoleViewModel> Users { get; set; }

        public OrganizationNavigationViewModel Navigation { get; set; }
    }
}
