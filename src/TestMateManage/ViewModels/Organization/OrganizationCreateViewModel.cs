﻿using System.ComponentModel.DataAnnotations;

namespace TestMateManage.ViewModels
{
    public class OrganizationCreateViewModel
    {
        [Required]
        [StringLength(18, MinimumLength = 3)]
        public string Name { get; set; }

        [StringLength(500, MinimumLength = 10)]
        public string Description { get; set; }
    }
}
