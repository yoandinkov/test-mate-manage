﻿using System.Collections.Generic;

namespace TestMateManage.ViewModels
{
    public class OrganizationPendingMembershipsViewModel
    {
        public OrganizationNavigationViewModel Navigation { get; set; }

        public IEnumerable<OrganizationRoleViewModel> Memberships { get; set; }
    }
}
