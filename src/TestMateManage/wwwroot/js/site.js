﻿$(function () {
    $('body').on("click", "button.join-organization", function (event) {
        var target = $(event.target),
            role = target.data('role'),
            id = target.data('organizationId');

        var data = {
            Type: role == 'teacher' ? 1 : 2,
            OrganizationId: id
        }

        $.ajax({
            url: '/organization/join',
            type: 'POST',
            data: {
                data: JSON.stringify(data)
            }
        })
        .success(function (redirect) {
            window.location.href = '/organization/list';
        })
        .error(function (error) {
            console.log(error);
        });
    });

    $('body').on("click", "button.approve-membership", function (event) {
        var target = $(event.target),
            roleId = target.data('role-id'),
            orgId = target.data('org-id');

        var data = {
            orgId: orgId,
            roleId: roleId
        }

        $.ajax({
            url: '/organization/approvemembership/' + orgId,
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8'
        })
        .success(function (redirect) {
            window.location.href = window.location.href;
        })
        .error(function (error) {
            console.log(error);
        });
    });

    $('body').on("click", "button.deny-membership", function (event) {
        var target = $(event.target),
            roleId = target.data('role-id'),
            orgId = target.data('org-id');

        var data = {
            orgId: orgId,
            roleId: roleId
        }

        $.ajax({
            url: '/organization/denymembership/' + orgId,
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
        })
        .success(function (redirect) {
            window.location.href = window.location.href;
        })
        .error(function (error) {
            console.log(error);
        });
    });

    $('body').on('click', 'button#create-assignment', function (event) {
        var target = $(event.target),
            orgId = target.data('org-id'),
            userIds = [],
            templateIds = [];

        $('.template-checkbox:checked').each(function (index, element) {
            templateIds.push({
                id: $(element).data('template-id'),
                title: $(element).data('template-title')
            });
        });

        $('.user-checkbox:checked').each(function (index, element) {
            userIds.push($(element).data('user-id'))
        });

        var data = {
            userIds: userIds,
            templateIds: templateIds
        }

        if (templateIds.length !== 0 && userIds.length !== 0) {
            $.ajax({
                type: 'POST',
                url: '/assignment/saveassignment/' + orgId,
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
            })
            .success(function (redirect) {
                window.location.href = '/organization/management/' + orgId;
            })
            .error(function (error) {
                console.log(error);
            });
        }
    });

    $('body').on('click', 'button#save-score', function (event) {
        var target = $(event.target),
          orgId = target.data('org-id'),
          evaluationId = target.data('evaluation-id'),
          score = 0,
          parsed = true;

        $('.generated-score').each(function (index, node) {
            var currentScore = parseFloat($(node).text());

            if (isNaN(currentScore)) {
                parsed = false;
            }

            score += currentScore;
        });

        $('.answer-score').each(function (index, node) {
            var currentScore = parseFloat($(node).val());

            if (isNaN(currentScore)|| currentScore < 0) {
                parsed = false;
            }

            score += currentScore;
        });

        if (parsed == false) {
            alert("There is negative or non numerical score. Please try again.");
            return;
        }

        var data = {
            id: evaluationId,
            score: score
        }

        $.ajax({
            type: "POST",
            url: "/assignment/save/" + orgId,
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8'
        })
        .success(function () {
            window.location.href = '/assignment/doneassignments/' + orgId 
        })
        .error(function () {
            console.log('error');
        })
    })
});
